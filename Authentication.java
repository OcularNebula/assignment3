import javax.swing.*;

public class Authentication{
	public static void main(String[] args){

	//set all the variables for input user data and correct user data
	String corAdminUser = new String ("Admin");
	String corAdminPW = new String ("123");
	String corStudentUser = new String ("BigDaddy");
	String corStudentPW = new String ("3thugz");
	String corStaffUser = new String ("Leon");
	String corStaffPW = new String ("2015");
	String[] choices = {"Admin", "Student", "Staff"};
	String aInput = new String();
	String uInput = new String();
	String pInput = new String();

	//set iteration parameters
	int trial = 0;
	final int LIMIT = 2;
	boolean check = true;

	//prompt user for Account Type
	while(check == true){

	aInput = (String) JOptionPane.showInputDialog(null, "Choose account type...", "Account Type", JOptionPane.QUESTION_MESSAGE, null, choices, choices[2]);

	//switch method for 3 different Account Types sequentially checks equality of input to correct username and password for each Account Type (up to 3 total incorrect inputs)
	switch(aInput){

	case "Admin":
		if(trial < LIMIT){
			uInput = JOptionPane.showInputDialog(null, "Enter Username: ");

			while((!uInput.equals(corAdminUser)) && (trial < LIMIT)){
				JOptionPane.showMessageDialog(null, "Incorrect Username. ");
				uInput = JOptionPane.showInputDialog(null, "Enter Username: ");
				trial++;
				}
			}
		if((trial < LIMIT) && (uInput.equals(corAdminUser))){
			pInput = JOptionPane.showInputDialog(null, "Enter Password: ");
	
			while((!pInput.equals(corAdminPW)) && (trial < LIMIT)){
				JOptionPane.showMessageDialog(null, "Incorrect Password. ");
				pInput = JOptionPane.showInputDialog(null, "Enter Password: ");
				trial++;
				}
			}
		if((trial < LIMIT) && (uInput.equals(corAdminUser)) && (pInput.equals(corAdminPW))){
			JOptionPane.showMessageDialog(null, "Welcome, Admin! You can update, read, add, and delete files.");
                        check = false;
			}
		else{
			JOptionPane.showMessageDialog(null, "Contact Administrator. ");
			trial = 0;
			}
	break;

	case "Student":
		if(trial < LIMIT){
                        uInput = JOptionPane.showInputDialog(null, "Enter Username: ");

                        while((!uInput.equals(corStudentUser)) && (trial < LIMIT)){
                                JOptionPane.showMessageDialog(null, "Incorrect Username. ");
				uInput = JOptionPane.showInputDialog(null, "Enter Username: ");
				trial++;
                                }
                        }
                if((trial < LIMIT) && (uInput.equals(corStudentUser))){
                        pInput = JOptionPane.showInputDialog(null, "Enter Password: ");

                        while((!pInput.equals(corStudentPW)) && (trial < LIMIT)){
                                JOptionPane.showMessageDialog(null, "Incorrect Password. ");
				pInput = JOptionPane.showInputDialog(null, "Enter Password: ");
				trial++;
                                }
                        }
		if((trial < LIMIT) && (uInput.equals(corStudentUser)) && (pInput.equals(corStudentPW))){
			JOptionPane.showMessageDialog(null, "Welcome, Student! You can only read files.");
			check = false;
			}
		else{
			JOptionPane.showMessageDialog(null, "Contact Administrator. ");
			trial = 0;
			}
        break;
	
	case "Staff":
		if(trial < LIMIT){
                        uInput = JOptionPane.showInputDialog(null, "Enter Username: ");

                        while((!uInput.equals(corStaffUser)) && (trial < LIMIT)){
                                JOptionPane.showMessageDialog(null, "Incorrect Username. ");
				uInput = JOptionPane.showInputDialog(null, "Enter Username: ");
				trial++;
                                }
                        }
                if(trial < LIMIT){
                        pInput = JOptionPane.showInputDialog(null, "Enter Password: ");

                        while((!pInput.equals(corStaffPW)) && (trial < LIMIT)){
                                JOptionPane.showMessageDialog(null, "Incorrect Password. ");
				pInput = JOptionPane.showInputDialog(null, "Enter Password: ");
				trial++;
                                }
                        }
		if((trial < LIMIT) && (uInput.equals(corStaffUser)) && (pInput.equals(corStaffPW))){
			JOptionPane.showMessageDialog(null, "Welcome, Staff! You can update and read files.");
			check = false;
			}
                else{
                        JOptionPane.showMessageDialog(null, "Contact Administrator. ");
			trial = 0;
                        }
        break;
		}
		}
	}
}
